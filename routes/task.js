// membuat variable router dengan require atau export variabel express
// Dan menggunakan metode Router
const router = require("express").Router();
// export controller yang ingin dipakai
const taskController = require("../controllers/taskController");

// endpoint mahasiswa
router.get("/", taskController.viewTask); // Untuk view
router.post("/", taskController.addTask); // Untuk menambahkan data task
router.put("/", taskController.editTask); // Untuk edit data task
router.delete("/:id", taskController.deleteTask); // Untuk hapus data mahasiswa berdasarkan id

// Lalu export routernya
module.exports = router;
