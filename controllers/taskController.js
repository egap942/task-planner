
const Task = require("../models/Task");

module.exports = {
 
  viewTask: async (req, res) => {
    try {
      
      const task = await Task.find();
      const alertMessage = req.flash("alertMessage");
      const alertStatus = req.flash("alertStatus");
      const alert = { message: alertMessage, status: alertStatus };
      
      res.render("index", {
        task,
        alert,
        title: "CRUD", 
      });
    } catch (error) {
      
      res.redirect("/task");
    }
  },

  
  addTask: async (req, res) => {
    
    try {
      
      const { note, start_time, end_time, create_by, started_by, finished_by, is_Active } = req.body;
      
      await Task.create({ note, start_time, end_time, create_by, started_by, finished_by, is_Active });
     
      req.flash("alertMessage", "Success add Task");
      req.flash("alertStatus", "success");
      res.redirect("/task"); 

    } catch (error) {
      
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
     
      res.redirect("/task");
    }
  },

  
  editTask: async (req, res) => {
    try {
      
      const { id, note, start_time, end_time, create_by, started_by, finished_by, is_Active } = req.body;
      const task = await Task.findOne({ _id: id });
      
      task.note = note;
      task.start_time = start_time;
      task.end_time = end_time;
      task.create_by = create_by;
      task.started_by = started_by;
      task.finished_by = finished_by;
      task.is_Active = is_Active;
      
      await task.save();
      
      req.flash("alertMessage", "Success edit Task");
      req.flash("alertStatus", "success");
      
      res.redirect("/task");
    } catch (error) {
      
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
     
      res.redirect("/task");
    }
  },

 
  deleteTask: async (req, res) => {
    try {
     
      const { id } = req.params;
      const task = await Task.findOne({ _id: id });
      
      await task.remove();
      
      req.flash("alertMessage", "Success delete Task");
      req.flash("alertStatus", "warning");
      res.redirect("/task");

    } catch (error) {
      
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/task");
    }
  },
};
