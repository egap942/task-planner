
const User = require("../models/User");


module.exports = {
  
  viewUser: async (req, res) => {
    try {
      
      const user = await User.find();
      
      const alertMessage = req.flash("alertMessage");
      const alertStatus = req.flash("alertStatus");
     
      const alert = { message: alertMessage, status: alertStatus };
     
      res.render("user/index", {
        user,
        alert,
        title: "CRUD", 
      });
    } catch (error) {
      
    }
  },

  loginUser: async (req, res) => {
   
    const { username, password } = req.body;
      
      console.log(req.body.email)
      if(req.body.email=="ega@gmail.com"&& req.body.password=="ega"){
   
        res.redirect("/task");
    }else{
        
        res.redirect("/");
    }
  },
  addUser: async (req, res) => {
   
    try {
      
      const { username, password, is_Active } = req.body;
      
      await User.create({ username, password, is_Active });
      
      req.flash("alertMessage", "Success add User");
      req.flash("alertStatus", "success");
      res.redirect("/user"); 
    } catch (error) {
      
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      
      res.redirect("/user");
    }
  },

  
  editUser: async (req, res) => {
    try {
     
      const { id, username, password, is_Active } = req.body;
      
      const User = await User.findOne({ _id: id });
     
      user.username = username;
      user.password = password;
      user.is_Active = is_Active;
     
     
      
      await user.save();
      
      req.flash("alertMessage", "Success edit User");
      req.flash("alertStatus", "success");
      
      res.redirect("/user");
    } catch (error) {
      
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      
      res.redirect("/user");
    }
  },

  
  deleteUser: async (req, res) => {
    try {
      
      const { id } = req.params;
     
      const user = await User.findOne({ _id: id });
      
      await user.remove();
     
      req.flash("alertMessage", "Success delete User");
      req.flash("alertStatus", "warning");
      
      res.redirect("/user");
    } catch (error) {
      
      req.flash("alertMessage", `${error.message}`);
      req.flash("alertStatus", "danger");
      
      res.redirect("/user");
    }
  },
};
