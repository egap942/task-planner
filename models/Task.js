const mongoose = require("mongoose");


const taskScheme = new mongoose.Schema({
  note: {
  
    type: String,
    
    required: true,
  },
  start_time: {
    
    type: Date,
    required: true,
  },
  end_time: {
    type: Date,
    required: true,
  },

  create_by: {
    type: String,
    required: true,
    
  },

  started_by: {
    type: String,
    required: true,
    
  },

  finished_by: {
    type: String,
    required: true,
    
  },

   is_Active: {
     type: Boolean,
    required: true,
    
   },
  
});


module.exports = mongoose.model("Task", taskScheme);
