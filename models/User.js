const mongoose = require("mongoose");


const userScheme = new mongoose.Schema({
  username: {
   
    type: String,
  
    required: true,
  },
  password: {
   
    type: String,
    required: true,
  },

  is_Active: {
   
    type: Boolean,
    required: true,
  },

 
});


module.exports = mongoose.model("User", userScheme);
